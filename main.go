package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"runtime"
	"sync"
	"time"

	"github.com/Route42/ct-lib/service"
	"github.com/spf13/viper"
)

func main() {
	getConfig()
	name := viper.GetString("name")
	useRoute42Service(name)

}

type Test struct {
	counter uint64
	secret  string
	done    chan bool
}

func useRoute42Service(name string) {
	if name == "" {
		log.Printf("config file has no name, fatal error")
		os.Exit(1)
	}
	log.Printf("found name %s", name)

	s := service.New(name)

	if s == nil {
		log.Printf("no service, fatal error")
		os.Exit(1)
	}

	log.Printf("starting work")

	result, err := s.Work()

	if err != nil {
		log.Printf("work service failed with: %v", err)
	}

	if result == nil {
		log.Printf("no result, trying again")
		os.Exit(1)
	}

	isValid := result.IsValid()

	if isValid {
		var wg sync.WaitGroup
		wg.Add(1)
		now := time.Now()
		go func() {
			defer func() {
				requestSubmitResultsWithTimeout(context.Background(), s, result)
				defer wg.Done()
			}()
		}()
		wg.Wait()
		fmt.Println("elapsed:", time.Since(now))

		fmt.Println("number of goroutines:", runtime.NumGoroutine())
	} else {
		log.Printf("invalid result")
	}
}

func requestSubmitResultsWithTimeout(ctx context.Context, s *service.Service, result *service.Result) error {
	ctx, cancel := context.WithTimeout(ctx, time.Second*30)
	defer cancel()

	done := make(chan error, 1)
	panicChan := make(chan interface{}, 1)
	go func() {
		defer func() {
			if p := recover(); p != nil {
				panicChan <- p
			}
		}()

		done <- submitResults(s, result)
	}()

	select {
	case err := <-done:
		log.Printf("chan done")
		return err
	case p := <-panicChan:
		log.Printf("panicChan")
		panic(p)
	case <-ctx.Done():
		log.Printf("ctx.done")
		return ctx.Err()
	}
}

func submitResults(s *service.Service, result *service.Result) error {
	s.SubmitResult(result)
	s.Wait()
	return nil
}

func getConfig() {
	viper.SetConfigName("config") // name of config file (without extension)
	viper.SetConfigType("yaml")   // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath(".")      // optionally look for config in the working directory
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println("fatal error config file: default \n", err)
		os.Exit(1)
	}
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			// Config file not found; ignore error if desired
			fmt.Println("config file not found, please create one")
			os.Exit(1)
		} else {
			fmt.Println("config file found, but something is wrong", err)
			os.Exit(1)
		}
	}

}
